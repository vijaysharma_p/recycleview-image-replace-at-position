package com.eryushion.recycleviewimageselect;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {
    MainActivity mainActivity;
    List<String> imagelist;
    MyClick myClick;

    public ImageAdapter(MainActivity mainActivity, List<String> imagelist, MyClick activity) {
        this.mainActivity = mainActivity;
        this.imagelist = imagelist;
        this.myClick = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_images, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        Glide.with(mainActivity).load(imagelist.get(position)).into(holder.image_view);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myClick != null) {
                    myClick.myClick(position);
                }
            }
        });
    }

    public void setSelectData(int position, String image) {
        imagelist.remove(position);
        imagelist.add(position, image);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image_view = itemView.findViewById(R.id.image_view);
        }
    }

    public interface MyClick {
        void myClick(int position);
    }
}
