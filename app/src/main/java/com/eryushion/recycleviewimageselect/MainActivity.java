package com.eryushion.recycleviewimageselect;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.dhaval2404.imagepicker.ImagePicker;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ImageAdapter.MyClick {

    public static List<String> imagelist;

    private RecyclerView recycleView;
    private ImageAdapter imageAdapter;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagelist = new ArrayList<>();
        imagelist.add(String.valueOf(R.mipmap.ic_launcher));
        imagelist.add(String.valueOf(R.mipmap.ic_launcher));
        imagelist.add(String.valueOf(R.mipmap.ic_launcher));
        imagelist.add(String.valueOf(R.mipmap.ic_launcher));

        recycleView = findViewById(R.id.recycleView);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        imageAdapter = new ImageAdapter(this, imagelist, MainActivity.this);
        recycleView.setAdapter(imageAdapter);

    }

    @Override
    public void myClick(int position) {
        this.position = position;
        ImagePicker.Companion.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            imagelist.remove(position);
            imagelist.add(position, ImagePicker.Companion.getFilePath(data));
            imageAdapter.setSelectData(position, ImagePicker.Companion.getFilePath(data));
        }
    }
}
